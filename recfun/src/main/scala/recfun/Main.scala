package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || r == c) 1
    else pascal(c-1, r-1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def balanced(opened: Int, chars: List[Char]): Boolean = {
      if(chars.isEmpty) 0 == opened
      else if(chars.head == '(') balanced(opened + 1, chars.tail)
      else if(chars.head == ')') opened > 0 && balanced(opened - 1, chars.tail)
      else balanced(opened, chars.tail)
    }
    balanced(0, chars)
  }

  /**
   * Exercise 3
   */
  // money 4
  // coins: List(1,2))
  // 3 ways (1+1+1+1, 1+1+2, 2+2)
  def countChange(money: Int, coins: List[Int]): Int = {
    def sortedChange(money: Int, coins: List[Int]): Int = {
      if (0 == money) 1
      else if (money < 0 || coins.isEmpty) 0
      else sortedChange(money - coins.head, coins) + sortedChange(money, coins.tail)
    }
    sortedChange(money, coins.sorted)
  }
}

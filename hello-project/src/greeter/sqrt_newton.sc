package greeter

object sqrt_newton {
  def abs(x: Double): Double = if (x < 0) -x else x
                                                  //> abs: (x: Double)Double
  // The isGoodEnough test is not very precise for small numbers
  // and can lead to non-termination for very large numbers. Explain why.
  //
  // * it is not very precise because of 0.001 preciseness
  // * non-termination on big number: It could be that the distance between one number and the next is actually larger than 0.001
	  
	
	def sqrt(x: Double) = {
	  def isGoodEnough(guess: Double): Boolean =
	    //abs(guess * guess - x) < 0.001
	    abs(guess * guess - x) / x < 0.001
	
	  def improve(guess: Double): Double =
	    (guess + x / guess) / 2
	  
		def sqrtIter(guess: Double): Double =
		  if (isGoodEnough(guess)) guess
		  else sqrtIter(improve(guess))
		  
		sqrtIter(1.0)
	}                                         //> sqrt: (x: Double)Double
	  
	sqrt(3)                                   //> res0: Double = 1.7321428571428572
	sqrt(0.001)                               //> res1: Double = 0.03162278245070105
	sqrt(0.1e-20)                             //> res2: Double = 3.1633394544890125E-11
	sqrt(1.0e20)                              //> res3: Double = 1.0000021484861237E10
  sqrt(1.0e60)                                    //> res4: Double = 1.0000788456669446E30
  
  
  // tail recursion lecture
  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)              //> gcd: (a: Int, b: Int)Int
    
  gcd(2,4)                                        //> res5: Int = 2
  
  //@tailrec
  def factorial(x: Int): Int =
    if (x == 0) 1 else x * factorial(x-1)         //> factorial: (x: Int)Int
  factorial(3)                                    //> res6: Int = 6
  factorial(4)                                    //> res7: Int = 24
  factorial(25)                                   //> res8: Int = 2076180480
  
}
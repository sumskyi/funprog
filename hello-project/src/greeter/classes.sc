package greeter

class Rational(x: Int, y: Int) {
	require(y != 0, "Denominator must be nonzero")
	
	// alternative constructor: new Rational(2) => new Rational(2, 1)
	def this(x: Int) = this(x, 1)
	
	private def gcd(a: Int, b: Int):Int = if (b == 0) a else gcd(b, a % b)
	//private val g = gcd(x, y)
	
	def numer = x // / g
	def denom = y // / g
	
	def < (that: Rational) = numer * that.denom < that.numer * denom
	def max(that: Rational) = if (this < that) that else this
	
	def +(that: Rational) =
		new Rational(
			numer * that.denom + that.numer * denom,
			denom * that.denom
		)
		
	def - (that: Rational) = this + -that
	//def - (that: Rational) = this + that.neg
		//new Rational(
		//	numer * that.denom - that.numer * denom,
		//	denom * that.denom
		//)
	def unary_- : Rational = new Rational(-numer, denom)
		
	def neg = new Rational(-numer, denom)
		
	// override def toString =  numer + "/" + denom
	override def toString =  {
		val g = gcd(numer, denom)
		numer / g + "/" + denom / g
	}
	
	
}

object classes {
	val x = new Rational(1, 3)                //> x  : greeter.Rational = 1/3
	x.numer                                   //> res0: Int = 1
	x.denom                                   //> res1: Int = 3
	x.neg                                     //> res2: greeter.Rational = 1/-3
	
	val y = new Rational(5, 7)                //> y  : greeter.Rational = 5/7
	val z = new Rational(3, 2)                //> z  : greeter.Rational = 3/2
	
	x - y - z                                 //> res3: greeter.Rational = -79/42
	y + y                                     //> res4: greeter.Rational = 10/7
	z < y                                     //> res5: Boolean = false
	y < z                                     //> res6: Boolean = true
	
	// x.max(y)
	x max y                                   //> res7: greeter.Rational = 5/7
	
	new Rational(2)                           //> res8: greeter.Rational = 2/1
	
	// val strange = new Rational(1, 0)
	// strange.add(strange)
	'zalupa                                   //> res9: Symbol = 'zalupa
	
	// operator's priority
	// ((a + b) ^? (c ?^ d)) less ((a ==> b) | c)
}
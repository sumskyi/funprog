package greeter

object tail_recursion {

	//@tailrec
	def factorial(n: Int): Int = {
	  def loop(acc: Int, n: Int): Int =
	  	if (n == 0) acc
	  	else loop(acc * n, n - 1)
	  loop(1, n)
	}                                         //> factorial: (n: Int)Int
	
	factorial(35)                             //> res0: Int = 0
	
	for (row <- 0 to 10) {
    for (col <- 0 to row)
      print(List(col, row) + " ")                 //> List(0, 0) List(0, 1) List(1, 1) List(0, 2) List(1, 2) List(2, 2) List(0, 3)
                                                  //|  List(1, 3) List(2, 3) List(3, 3) List(0, 4) List(1, 4) List(2, 4) List(3, 4
                                                  //| ) List(4, 4) List(0, 5) List(1, 5) List(2, 5) List(3, 5) List(4, 5) List(5, 
                                                  //| 5) List(0, 6) List(1, 6) List(2, 6) List(3, 6) List(4, 6) List(5, 6) List(6,
                                                  //|  6) List(0, 7) List(1, 7) List(2, 7) List(3, 7) List(4, 7) List(5, 7) List(6
                                                  //| , 7) List(7, 7) List(0, 8) List(1, 8) List(2, 8) List(3, 8) List(4, 8) List(
                                                  //| 5, 8) List(6, 8) List(7, 8) List(8, 8) List(0, 9) List(1, 9) List(2, 9) List
                                                  //| (3, 9) List(4, 9) List(5, 9) List(6, 9) List(7, 9) List(8, 9) List(9, 9) Lis
                                                  //| t(0, 10) List(1, 10) List(2, 10) List(3, 10) List(4, 10) List(5, 10) List(6,
                                                  //|  10) List(7, 10) List(8, 10) List(9, 10) List(10, 10) 
  }
                                                  
  def sabaka(x: Any):Any = if (true) 2 else "вы все пидарасы"
                                                  //> sabaka: (x: Any)Any
                                                  
  sabaka(3)                                       //> res1: Any = 2
}
package greeter

object ex2 {
  def sum(f: Int => Int, a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, f(a) + acc)
    }
    loop(a, 0)
  }                                               //> sum: (f: Int => Int, a: Int, b: Int)Int
  

  sum(x => x * x, 3, 5)                           //> res0: Int = 50
  // loop(3, 0)
  // loop(4, 4*4+0)
  // loop(4, 16)
  // loop(5, 16)
  // loop(6, 25 + 16)
  // loop(6, 41)
  // loop(7, 36+41) ?????????????? WTF
  sum(x => x * x, 2, 3)                           //> res1: Int = 13
  // loop(2, 0)
  // loop(3, 4+0)
  // loop(4, 9+4)
  // 13
  sum(x => x * x, 3, 4)                           //> res2: Int = 25
  
  
  
  // CURRYING
  // Write a product function that
  // calculates the product of the values of a function for the points on a given interval.
  def product(f: Int => Int)(a: Int, b: Int):Int =
    if(a > b) 1
    else f(a) * product(f)(a +1, b)               //> product: (f: Int => Int)(a: Int, b: Int)Int
  
  product(x => x * x)(3, 4)                       //> res3: Int = 144
  
  // factorial in terms of product
  def fact(n: Int) = product(x => x)(1, n)        //> fact: (n: Int)Int
  
  fact(4)                                         //> res4: Int = 24
  
  // Can you write a more general function, which generalizes both sum and product?
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int):Int =
    if(a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a + 1, b))
                                                  //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b
                                                  //| : Int)Int
  def product2(f: Int => Int)(a: Int, b: Int):Int =
    mapReduce(f, (x, y) => x * y, 1)(a, b)        //> product2: (f: Int => Int)(a: Int, b: Int)Int
  
   product2(x => x * x)(3, 4)                     //> res5: Int = 144
  
}
package greeter

object worksheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

	val x = 1                                 //> x  : Int = 1
	def increase(i: Int) = i + 1              //> increase: (i: Int)Int
	increase(x)                               //> res0: Int = 2
	
	List(1, 2, 3)(1)                          //> res1: Int = 2
	
	"(just an) example".toList                //> res2: List[Char] = List((, j, u, s, t,  , a, n, ),  , e, x, a, m, p, l, e)
	
     
  List(500,5,50,100,20,200,10).sorted             //> res3: List[Int] = List(5, 10, 20, 50, 100, 200, 500)
  
  println("2")                                    //> 2

}
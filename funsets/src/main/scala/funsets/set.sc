package funsets

object set {
  type Set = Int => Boolean
  
  def singletonSet(elem: Int): Set =
    x => x == elem                                //> singletonSet: (elem: Int)Int => Boolean
    
  def contains(s: Set, elem: Int): Boolean = s(elem)
                                                  //> contains: (s: Int => Boolean, elem: Int)Boolean
  
  // /////
  val bound = 1000                                //> bound  : Int = 1000
  
  def toString(s: Set): String = {
    val xs = for (i <- -bound to bound if contains(s, i)) yield i
    xs.mkString("{", ",", "}")
  }                                               //> toString: (s: Int => Boolean)String
  
  def printSet(s: Set) {
    println(toString(s))
  }                                               //> printSet: (s: Int => Boolean)Unit

  val qq = singletonSet(2)                        //> qq  : Int => Boolean = <function1>
  printSet(qq)                                    //> {2}
  contains(qq, 2)                                 //> res0: Boolean = true
}